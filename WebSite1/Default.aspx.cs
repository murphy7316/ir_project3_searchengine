﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MongoDB.Driver;
using MongoDB.Bson;
using System.Collections;
using MongoDB.Bson.Serialization.Attributes;
using MongoDB.Driver.Builders;
using MongoDB;
using System.Threading;
using System.IO;

public partial class _Default : System.Web.UI.Page 
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }
    /*
    public class MultiMap<V>
    {
        Dictionary<string, List<V>> _dictionary =
        new Dictionary<string, List<V>>();

        public void Add(string key, V value)
        {
            List<V> list;
            if (this._dictionary.TryGetValue(key, out list))
            {
                list.Add(value);
            }
            else
            {
                list = new List<V>();
                list.Add(value);
                this._dictionary[key] = list;
            }
        }

        public IEnumerable<string> Keys
        {
            get
            {
                return this._dictionary.Keys;
            }
        }

        public List<V> this[string key]
        {
            get
            {
                List<V> list;
                if (!this._dictionary.TryGetValue(key, out list))
                {
                    list = new List<V>();
                    this._dictionary[key] = list;
                }
                return list;
            }
        }
    }
    */
    public class score
    {
        public MongoDB.Bson.ObjectId _id { get; set; }
        public string url { get; set; }
        public int tfidf { get; set; }
    }

    protected void Button1_Click(object sender, EventArgs e)
    {
        TextBox3.Text = "";
        var server = MongoServer.Create("mongodb://ir:123@192.168.1.138:27017/");
        if (server.State == MongoServerState.Disconnected)
            server.Connect();
        var OneConsoleDB = server.GetDatabase("One");
        if (!OneConsoleDB.CollectionExists("NextMessages"))
            OneConsoleDB.CreateCollection("NextMessages", null);
        var NextMessages = OneConsoleDB.GetCollection("NextMessages");

        MongoDatabase db = server.GetDatabase("irproj3db");
        MongoCollection<BsonDocument> CoSscore = db.GetCollection<BsonDocument>("invertedIndex");
        MongoCollection<BsonDocument> pageScore = db.GetCollection<BsonDocument>("pageRank");
        string[] line;
        //string[] tmpline;

        int numberOfDocument=0;
        //int isFirstStr = 0;
        line = TextBox1.Text.Split(null);
        /*
        foreach (string isTermExistStr in line.Distinct())
        {
            int isTermExist = 0;
            var tmpQuery = Query.EQ("term", isTermExistStr);
            isTermExist = int.Parse(CoSscore.Find(tmpQuery).Count().ToString());
            if (isTermExist == 0) {
                if (isFirstStr == 0){
                    tmpline = tmpline.ToString().Remove(0,isTermExistStr.Length + 1);
                }
                else{ 
            
                }
            }
            isFirstStr++;
        }
        */
        int noDupTerm=0;
        foreach(string strlist in line.Distinct()){
            noDupTerm++;       
        }
        //MultiMap<string> m1 = new MultiMap<string>();
        //MultiMap<string> m2 = new MultiMap<string>();
        //calcualte input term frequency
        Dictionary<string, double> countInputTerm = new Dictionary<string, double>();

        foreach (string str_countInputTerm in line) {
            if (countInputTerm.ContainsKey(str_countInputTerm.ToLower().ToString()) == true){
                countInputTerm[str_countInputTerm.ToLower().ToString()]++;
            }else{
                countInputTerm.Add(str_countInputTerm.ToLower().ToString(), 1);
            }
        }
        int numberOfTerm=0;
        int flag = 1;

        Dictionary<string, double[]> dict = new Dictionary<string, double[]>();
        //dict.Add("apple", new int[] { 1, 2, 3 });
        //int value[]=dict["apple"];

        foreach (string tmp in line)
        {
            for (int i = 0; i < numberOfTerm; i++) {
                if (tmp.ToLower().ToString() == line[i])
                {
                    flag = 0;
                    break;
                }
                else {
                    flag = 1;
                }
            }
            if (flag == 1)
            {
                var query = Query.EQ("term", tmp.ToLower().ToString());
                //var query = Query.ElemMatch("urls.url", Query.EQ("term", tmp.ToLower().ToString()));
                numberOfDocument = 0;
                foreach (BsonDocument score in CoSscore.Find(query))
                {
                    //var address = nested["address"].AsBsonDocument;
                    //Console.WriteLine(address["city"].AsString);
                    //TextBox3.Text = score["doc_freq"].ToString();
                    //TextBox3.Text += score["urls"].AsBsonArray.Count().ToString();
                    //for (int i = 0; i < score["doc_freq"]; i++)
                    for (int i = 0; i < score["urls"].AsBsonArray.Count(); i++)
                    {
                        //TextBox3.Text += score["urls"][i][0].ToString();
                        //m1.Add(score["urls"][i][0].ToString(), score["urls"][i][1].ToString());
                        if (dict.ContainsKey(score["urls"][i][0].ToString())){
                            dict[score["urls"][i][0].ToString()][numberOfTerm] = double.Parse(score["urls"][i][4].ToString());
                        }
                        else {
                            dict.Add(score["urls"][i][0].ToString(), new double[noDupTerm]);
                            dict[score["urls"][i][0].ToString()][numberOfTerm] = double.Parse(score["urls"][i][4].ToString());
                        }
                        
                        //for (int j = 0; j < noDupTerm-1; j++)
                        //{
                        //    m1.Add(score["urls"][i][0].ToString(), zero.ToString());
                        //}
                    }
                    //var result = score["urls"][];
                    //TextBox3.Text = result["url"].ToString();
                    //m1.Add(score["urls"][i][0].ToString(), score["urls"][i][1].ToString());
                    //m1.Add(score["urls"].ge, score["doc_freq"].ToString());
                    //m1.Add(score["urls.url"].ToString(), score["urls.tfidf"].ToString());
                    numberOfDocument++;
                }
            numberOfTerm++;
            }
        }
        int tmpOrder=0;
        //int termFlag = 0;
        double[] inputTermScore=new double[countInputTerm.Keys.Count];
        //double[] inputTermScore = new double[2];
        foreach (KeyValuePair<string, double> item in countInputTerm){    
            var query = Query.EQ("term", item.Key.ToLower().ToString());
            int multipleDocFreq = 0;
            foreach (BsonDocument tmp in CoSscore.Find(query)){
                multipleDocFreq += int.Parse(tmp["urls"].AsBsonArray.Count().ToString());
            }    
            inputTermScore[tmpOrder] = Math.Round((1 + Math.Log10(item.Value / line.Length)) * Math.Log10(1000000 /multipleDocFreq), 3);
            tmpOrder++;  
            
        }
        double tmp_inputTermScore = 0;
        for (int i = 0; i < countInputTerm.Keys.Count; i++){
            tmp_inputTermScore += inputTermScore[i] * inputTermScore[i];
        }
        double tmp_documentScore = 0;
        double[] tmpScore = new double[countInputTerm.Keys.Count];
        var llist =dict.Keys.ToList();
        Dictionary<string, double> FinalCosineScore = new Dictionary<string, double>();
        foreach (var kk in llist){
            double scorePerDocument=0;
            for (int i = 0; i < numberOfTerm; i++){
               tmpScore[i] =inputTermScore[i]*dict[kk][i];
               tmp_documentScore += dict[kk][i] * dict[kk][i];
            }
            double tmpResult = 0;
            for (int c = 0; c < countInputTerm.Keys.Count; c++) {
                tmpResult += tmpScore[c];
            }
            scorePerDocument=Math.Round(tmpResult/(Math.Sqrt(tmp_documentScore)*Math.Sqrt(tmp_inputTermScore)),3);
            var queryPageRankScore = Query.EQ("url", kk);
            double tmpPageScore = 0;
            foreach (BsonDocument tmpPage in pageScore.Find(queryPageRankScore)) {
                tmpPageScore = double.Parse(tmpPage["rank"].ToString());
            }
            FinalCosineScore.Add(kk, scorePerDocument + tmpPageScore);
        }
        //var llist2 = FinalCosineScore.Keys.ToList();
        //foreach (var kk in llist2){
            //TextBox3.Text += kk + "=" + FinalCosineScore[kk].ToString() + "\n\r";
        //}

        var sortResult = from pair in FinalCosineScore
                         orderby pair.Value descending
                         select pair;
        foreach (KeyValuePair<string, double> pair in sortResult) {
            TextBox3.Text += pair.Key + "=" + pair.Value + "\n\r";
        }

        //var llist3 =dict.Keys.ToList();
        //foreach (var kk in llist3){
        //    for (int i = 0; i < numberOfTerm; i++){
        //        TextBox3.Text += kk + "=" + dict[kk][i].ToString() + " ";
        //    }
        //}
        //foreach (string k in m1.Keys)
        //{
            
            //TextBox2.Text += k;
        //    foreach (string v in m1[k])
         //   {
                //TextBox2.Text += k +"="+v;
        //    }
        //}
        
    }
}
